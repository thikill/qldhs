﻿using System.Web;
using MrCMS.Website;
using Ninject.Modules;

namespace MrCMS.Web.Tests
{
    public class TestContextModule : NinjectModule
    {	
        public override void Load()
        {
			//thucnx test
            Kernel.Rebind<HttpContextBase>().To<OutOfContext>().InThreadScope();
        }
    }
}